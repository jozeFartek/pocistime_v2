package com.example.joze.pocistime_v2.Data;

/**
 * Created by Joze on 4. 12. 2017.
 */

public class Tag {
    int Id;
    String name;

    public Tag(int id, String name) {
        Id = id;
        this.name = name;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
