package com.example.joze.pocistime_v2.eventbus;

import com.example.joze.pocistime_v2.Data.Odpadek;

/**
 * Created by Joze on 18. 12. 2017.
 */

public class MessageEventUpdateOdpadek {
    private Odpadek odpadek;

    public MessageEventUpdateOdpadek(Odpadek odpadek) {

        this.odpadek = odpadek;
    }
    public Odpadek getOdpadek() {
        return odpadek;
    }
}
