package com.example.joze.pocistime_v2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

public class AddOdpadekMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    MapView mapView;
    Button btnSave;

    private GoogleMap googleMap;
    double mLat = 0, mLon = 0;
    String city = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_odpadek_map);

        getSupportActionBar().setTitle("Izberi lokacijo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initGUI(savedInstanceState);
    }

    private void initGUI(Bundle savedInstanceState) {
        mapView = (MapView) findViewById(R.id.add_on_map_mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);//when you already implement OnMapReadyCallback in your fragment

        btnSave = (Button) findViewById(R.id.btn_add_on_map);
        btnSave.setVisibility(View.INVISIBLE);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AddOdpadekMapActivity.this, AddOdpadekActivity.class);
                i.putExtra(ApplicationMy.LAT,  mLat);
                i.putExtra(ApplicationMy.LON,  mLon);
                i.putExtra(ApplicationMy.CITY,  city);
                startActivity(i);
                finish();
            }
        });
    }

    void enableButton()
    {
        btnSave.setVisibility(View.VISIBLE);
    }

    void saveLocation(double lat, double lon)
    {
        mLat = lat;
        mLon = lon;
        try
        {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            city  = addresses.get(0).getLocality();
        }
        catch (Exception e)
        {

        }


    }

    // GOOGLE MAP
    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;


        LatLng coordinate = new LatLng(46.078947, 14.594238);

       /* if(mLocation != null)
        {
            coordinate = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        }*/


        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 8);
        //map.animateCamera(yourLocation);
        map.moveCamera(yourLocation);

        final GoogleMap fMap = map;
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
              //  lstLatLngs.add(point);
                enableButton();
                saveLocation(point.latitude, point.longitude);
                fMap.clear();
                fMap.addMarker(new MarkerOptions().position(point));
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


}
