package com.example.joze.pocistime_v2.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.location.Location;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.joze.pocistime_v2.ApplicationMy;
import com.example.joze.pocistime_v2.Data.Odpadek;
import com.example.joze.pocistime_v2.OdpadekActivity;
import com.example.joze.pocistime_v2.R;
import com.example.joze.pocistime_v2.Utils.ApiClient;
import com.example.joze.pocistime_v2.Utils.Util;

import java.util.ArrayList;

/**
 * Created by Joze on 4. 12. 2017.
 */

public class OdpadkiLikedAdapter extends RecyclerView.Adapter<OdpadkiLikedAdapter.ViewHolder> {
    private static String TAG = "OdpadkiLikedAdapter";


    ApplicationMy app;
    static Context context;
    ArrayList<Odpadek> odpadekArrayList = new ArrayList<>();

    Location last;
    public static int UPDATE_DISTANCE_IF_DIFF_IN_M=10;

    public OdpadkiLikedAdapter(ApplicationMy app) {
        super();
        this.app = app;

        odpadekArrayList = app.odpadkiLiked;

    }

    public void setLastLocation(Location l) {
        Log.e("TAG","new Location Adapter odpadek");
        if (last==null) {
            last = l;
            notifyDataSetChanged();
        }
        else {
            if (Util.distance(last.getLatitude(),last.getLongitude(),l.getLatitude(),l.getLongitude())>UPDATE_DISTANCE_IF_DIFF_IN_M){
                last = l;
                notifyDataSetChanged();
            }
        }
    }


    @Override
    public OdpadkiLikedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout_odlagalisce, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);

        context = parent.getContext();
        return vh;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTtitle;
        public TextView tvSubtitle;
        public CardView rowLayout;
        public TextView tvTime;
        public View statusView;
        public ImageView image;
        public ImageView ivDistance;
        public TextView tvDistance;


        public ViewHolder(View v) {
            super(v);
            tvTtitle = (TextView) v.findViewById(R.id.rl_title);
            tvSubtitle = (TextView) v.findViewById(R.id.rl_subtitle);
            rowLayout = (CardView) v.findViewById(R.id.row_layout_odpadek);
            statusView = (View) v.findViewById(R.id.status_line);
            tvTime = (TextView) v.findViewById(R.id.textView_time);
            image = (ImageView) v.findViewById(R.id.rl_odlagalisce_image);
            tvDistance = (TextView) v.findViewById(R.id.textView_distance);
            ivDistance = (ImageView) v.findViewById(R.id.imageView_odpadek_ad_distance);
         }



    }

    @Override
    public void onBindViewHolder(OdpadkiLikedAdapter.ViewHolder holder, int position) {
        final Odpadek odpadek = odpadekArrayList.get(position);

        Boolean load = false;
        if(odpadek.getFilePath() != null)
        {
            if(!odpadek.getFilePath().equals(""))
            {
                try {
                    Glide.with(context)
                            .load(ApiClient.BASE_URL + "/api/Images/" + odpadek.getId() + "/" )
                            .into(holder.image);
                    load = true;
                }
                catch (Exception e) {}
            }
        }

        if(!load)
        {
            Glide.with(context)
                    .load(R.drawable.no_image)
                    .into(holder.image);
        }

        holder.tvTtitle.setText(odpadek.getName());
        holder.tvSubtitle.setText(odpadek.getDescription());

        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OdpadkiLikedAdapter.startDView(odpadek.getId());
            }
        });

        String time = app.getTimeAgo(odpadek.getDateCreated());
        holder.tvTime.setText(time);

        if(odpadek.getStatusId() == 1)
            holder.statusView.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.status0)));
        else
            holder.statusView.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.status3)));


        // DISTANCE
        if (last==null)
            holder.tvDistance.setText("");
        else {
            holder.tvDistance.setText(Util.getDistanceInString(last.getLatitude(), last.getLongitude(),odpadek.getLatitude(),odpadek.getLongitude()));
            holder.ivDistance.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getItemCount(){
        return app.odpadkiLiked.size();
    }


    private static void startDView(int odpadek_id) {

        Log.i(TAG, "Odpadek id:" + odpadek_id);
        Intent i = new Intent(context, OdpadekActivity.class);
        i.putExtra(ApplicationMy.ODPADEK_ID,  odpadek_id);
        context.startActivity(i);

    }
}
