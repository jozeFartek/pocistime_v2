package com.example.joze.pocistime_v2;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.util.Log;

import com.example.joze.pocistime_v2.Data.Like;
import com.example.joze.pocistime_v2.Data.MyResponse;
import com.example.joze.pocistime_v2.Data.Odpadek;
import com.example.joze.pocistime_v2.Data.Uporabnik;
import com.example.joze.pocistime_v2.Utils.ApiClient;
import com.example.joze.pocistime_v2.Utils.ApiInterface;
import com.example.joze.pocistime_v2.Utils.Util;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateLocation;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateOdpadek;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateOdpadki;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jozef on 20. 11. 2017.
 */

public class ApplicationMy extends Application {
    private  static String TAG = "ApplicationMy";


    public static String ODPADEK_ID = "ODPADEK_ID";
    public static String UPORABNIK_ID = "UPORABNIK_ID";
    public ArrayList<Odpadek> odpadki = new ArrayList<>();
    public ArrayList<Odpadek> odpadkiLiked = new ArrayList<>();
    private Odpadek odpadek;

    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public static final String PREFS_USER_ID = "UserId";
    public static final String PREFS_USER_NICKNAME = "UserNickName";
    public static final String PREFS_USER_EMAIL = "UserEmail";
    public static final String PREFS_USER_IMAGE_PATH = "UserImagePath";

    public static final String LAT = "LAT";
    public static final String LON = "LON";
    public static final String CITY = "CITY";
    // https://stackoverflow.com/questions/23024831/android-shared-preferences-example

    Location mLocation;
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    private static final int SORT_BY_DATE=0;
    private static final int SORT_BY_DISTACE=1;
    int sortType = SORT_BY_DATE;


    public static String[] tags = {
            "Organski odpadki",
            "Gradbeni odpadki",
            "Komunalni odpadki",
            "Kosovni odpadki",
            "Pnevmatike",
            "Motorna vozila",
            "Nevarni odpadki",
            "Salonitne plošče",
            "Sodi z nevarno tekočino"
    };


    /** IMAGE */
    public static final String IMAGE_DIRECTORY_NAME = "imageuploadtest";




    @Override
    public void onCreate() {
        super.onCreate();
    }

    public String getStr1()
    {
        return "str1";
    }

    public String getStr2(){
        return  "str2";
    }


    public Location getLocation() {
        return mLocation;
    }

    public void requstServerOdpadki(){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<Odpadek>> call = apiService.getOdpadki();
        call.enqueue(new Callback<ArrayList<Odpadek>>() {
            @Override
            public void onResponse(Call<ArrayList<Odpadek>> call, Response<ArrayList<Odpadek>> response) {
                int statusCode = response.code();
                odpadki = response.body();

                EventBus.getDefault().post(new MessageEventUpdateOdpadki(odpadki));
                Log.i(TAG, response.body().toString() + "");

            }

            @Override
            public void onFailure(Call<ArrayList<Odpadek>> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    public void requstServerOdpadek(int id){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        Call<Odpadek> call = apiService.getOpdadek(id, new Uporabnik(getLoginId()));
        call.enqueue(new Callback<Odpadek>() {
            @Override
            public void onResponse(Call<Odpadek> call, Response<Odpadek> response) {
                int statusCode = response.code();
                Odpadek tmpOdpadek =  response.body();
                if(tmpOdpadek == null)
                    return;

                odpadek = response.body();
                EventBus.getDefault().post(new MessageEventUpdateOdpadek(odpadek));
                Log.i(TAG, response.body().toString() + "");

            }

            @Override
            public void onFailure(Call<Odpadek> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }



    public Odpadek getOdpadek() {
        return odpadek;
    }

    public void setOdpadki(ArrayList<Odpadek> odpadki) {
        this.odpadki = odpadki;
    }



    public int getLoginId()
    {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Integer userId = prefs.getInt(PREFS_USER_ID, -1);

        Log.i(TAG, "UserId: " + userId);
        return userId;
    }



    public Uporabnik getUserData()
    {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Integer userId = prefs.getInt(PREFS_USER_ID, -1);
        String email = prefs.getString(PREFS_USER_EMAIL, "");
        String nickName = prefs.getString(PREFS_USER_NICKNAME, "");
        String imagePath = prefs.getString(PREFS_USER_IMAGE_PATH, "");

        return new Uporabnik(userId, nickName, email, imagePath);
    }

    public void Logout()
    {
        odpadkiLiked = new ArrayList<>();
        SharedPreferences.Editor editor = getSharedPreferences(ApplicationMy.MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putInt(PREFS_USER_ID, -1);
        editor.putString(PREFS_USER_NICKNAME, "");
        editor.putString(PREFS_USER_EMAIL, "");
        editor.putString(PREFS_USER_IMAGE_PATH, "");
        editor.commit();
    }







    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();;
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }


    public void requstServerAddLike(int odpadekId){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Like like = new Like(odpadekId, getLoginId());
        Log.i(TAG, like.toString());
        Call<MyResponse> call = apiService.addLike(like);
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                int statusCode = response.code();
                MyResponse myResponse = response.body();
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    public void requstServerRemoveLike(int odpadekId){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Like like = new Like(odpadekId, getLoginId());
        Call<MyResponse> call = apiService.removeLike(like);
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                int statusCode = response.code();
                MyResponse myResponse = response.body();
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }


    public void sortUpdate() {
        //sortType= (sortType+1) / 2;
        switch (sortType) {
            case SORT_BY_DATE:{
                Log.e(TAG,"sort Time");
                Collections.sort(odpadki, new Comparator<Odpadek>() {
                    @Override
                    public int compare(Odpadek l1, Odpadek l2) {
                        if (l1.getDateCreated()==l2.getDateCreated()) return 0;
                        if (l1.getDateCreated()>l2.getDateCreated()) return -1;
                        return 1;
                    }
                });
            }
            break;
            case SORT_BY_DISTACE:{
                if (mLocation==null) return;
                Log.e(TAG,"sort Distance");
                Collections.sort(odpadki, new Comparator<Odpadek>() {
                    @Override
                    public int compare(Odpadek l1, Odpadek l2) {
                        int d1 = Util.distance(mLocation.getLatitude(),mLocation.getLongitude(),l1.getLatitude(),l1.getLongitude());
                        int d2 = Util.distance(mLocation.getLatitude(),mLocation.getLongitude(),l2.getLatitude(),l2.getLongitude());
                        if (d1==d2) return 0;
                        if (d1>d2) return 1;
                        return -1;
                    }
                });

            }
            break;
        }

    }
    public void sortChangeAndUpdate() {
        sortType= (sortType+1) % 2;
        sortUpdate();
    }



}
