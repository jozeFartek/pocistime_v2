package com.example.joze.pocistime_v2;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.joze.pocistime_v2.Data.MyResponse;
import com.example.joze.pocistime_v2.Data.Odpadek;
import com.example.joze.pocistime_v2.Data.Tag;
import com.example.joze.pocistime_v2.Data.Uporabnik;
import com.example.joze.pocistime_v2.Utils.ApiClient;
import com.example.joze.pocistime_v2.Utils.ApiInterface;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddOdpadekActivity extends AppCompatActivity {
    public static String TAG = "AddOdpadekActivity";

    public static final int REQUEST_CODE_CAMERA = 0012;
    public static final int REQUEST_CODE_GALLERY = 0013;
    ApplicationMy app;


    private Uri fileUri;
    private Button btnCapturePicture;
    private ImageView imageview;
    private EditText et_name;
    private EditText et_description;
    private TextView tv_location, tv_city;
    ChipCloud chipCloud;

    private Button btnSave;

    private ProgressDialog pDialog;

    boolean isImageUploaded = false;
    File mImageFile;
    private String [] items = {"Camera","Gallery"};

    Odpadek odpadek = new Odpadek();
    double mLat = 0, mLon = 0;
    String city = "";

    Map<Integer, Tag> map_tags = new HashMap<Integer, Tag>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_odpadek);

        getSupportActionBar().setTitle("Vpiši podatke");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras = getIntent().getExtras();
        mLat = extras.getDouble(ApplicationMy.LAT);
        mLon = extras.getDouble(ApplicationMy.LON);
        city = extras.getString(ApplicationMy.CITY, "");
        odpadek.setFilePath("");



        /** NALOŽI ApplicationMy */
        app = (ApplicationMy) getApplication();



        /** GUI **/
        loadGUI();



    }

    private void loadGUI() {
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        tv_location = (TextView) findViewById(R.id.addOpdadek_text_location);
        tv_location.setText("Lokacija: " + mLat + "," + mLon);

        tv_city = (TextView) findViewById(R.id.addOdpadek_text_city);
        tv_city.setText("Občina: " + city);

        et_name = (EditText) findViewById(R.id.editText_addOdpadek_name);
        et_description = (EditText) findViewById(R.id.editText_addOdpadek_opis);
        imageview = (ImageView) findViewById(R.id.addOdpadek_image);
        btnCapturePicture = (Button) findViewById(R.id.btn_take_image);
        btnCapturePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage();
            }
        });
        btnSave = (Button) findViewById(R.id.btn_addOdpadek_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
                if(isImageUploaded)
                        UpploadImageData();
                else
                     UploadOdpadekData();

            }
        });

        chipCloud = (ChipCloud) findViewById(R.id.chip_cloud);
        new ChipCloud.Configure()
                .chipCloud(chipCloud)
                .selectedColor(Color.parseColor("#388e3c"))
                .selectedFontColor(Color.parseColor("#ffffff"))
                .deselectedColor(Color.parseColor("#e1e1e1"))
                .deselectedFontColor(Color.parseColor("#333333"))
                .selectTransitionMS(500)
                .deselectTransitionMS(250)
                .labels(new String[]{})
                .mode(ChipCloud.Mode.MULTI)
                .allCaps(false)
                .gravity(ChipCloud.Gravity.CENTER)
                .textSize(getResources().getDimensionPixelSize(R.dimen.default_textsize))
                .verticalSpacing(getResources().getDimensionPixelSize(R.dimen.vertical_spacing))
                .minHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.min_horizontal_spacing))
                .chipListener(new ChipListener() {
                    @Override
                    public void chipSelected(int index) {
                        map_tags.put(index+1, new Tag(index+1, ""));
                    }
                    @Override
                    public void chipDeselected(int index) {
                        map_tags.remove(index+1);
                    }
                })
                .build();
        chipCloud.addChips(ApplicationMy.tags);
    }

    private void UploadOdpadekData() {
        String name = et_name.getText().toString();
        String description = et_description.getText().toString();
        odpadek.setId(-1);
        odpadek.setName(name);
        odpadek.setDescription(description);
        odpadek.setUserId(app.getLoginId());
        odpadek.setLatitude(mLat);
        odpadek.setLongitude(mLon);
        odpadek.setLocationAddress(city);



       /* ArrayList<Tag> tags = new ArrayList<>();
        for (int i = 0; i < chipCloud.getChildCount(); i++) {
            View view = chipCloud.getChildAt(i);
            if(view.isSelected(i))
            {
                tags.add(new Tag(i, ""));
                Log.i("MY_APP add2", "tag_selected: " + i);
            }
            else
                Log.i("MY_APP add2", "tag_selected not: " + i);

        }
        */
        ArrayList<Tag> tags = new ArrayList<>();
        for (Map.Entry<Integer, Tag> entry : map_tags.entrySet()) {
            Integer key = entry.getKey();
            Tag value = entry.getValue();
            tags.add(value);
            // ...
        }
        odpadek.setTags(tags);

        Log.i("MY_APP add2", odpadek.toString());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Odpadek> call = apiService.addOdpadek(odpadek);

        Log.i("MY_APP", "step1");
        call.enqueue(new Callback<Odpadek>() {
            @Override
            public void onResponse(Call<Odpadek> call, Response<Odpadek> response) {
                Log.i("MY_APP add", "step2");
                Odpadek odpadekRes = response.body();
                Log.i("MY_APP add", odpadekRes.toString());
                Log.i("MY_APP add", odpadek.toString());
                hideDialog();
                if(odpadekRes.getId() != -1)
                {
                    Intent i = new Intent(AddOdpadekActivity.this, OdpadekActivity.class);
                    i.putExtra(ApplicationMy.ODPADEK_ID,  odpadekRes.getId());
                    startActivity(i);
                    finish();
                }


            }

            @Override
            public void onFailure(Call<Odpadek> call, Throwable t) {
                // Log error here since request failed
                Log.i("MY_APP", "step3");
                Log.e(TAG, t.toString());
                hideDialog();
            }
        });




        
    }

    private void UpploadImageData()
    {
        File file = mImageFile;

        // create RequestBody instance from file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);

        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<MyResponse> call = apiService.uploadImage(fileToUpload);

        Log.i("MY_APP uppload", "step1");
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                int statusCode = response.code();
                MyResponse myResponse = response.body();
                hideDialog();
                Log.i("MY_APP path", "myResponse path:" + myResponse.getMessage());
                setFilePath(myResponse.getMessage());
                UploadOdpadekData();
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                // Log error here since request failed
                Log.i("MY_APP", "step3");
                Log.e(TAG, t.toString());
                hideDialog();
            }
        });


    }

    public void setFilePath(String path)
    {

        odpadek.setFilePath(path);
        Log.i("MY_APP path", "path:" + odpadek.getFilePath());
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    /**
     * this method used to open image directory or open from camera
     */
    private void openImage(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Options");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(items[i].equals("Camera")){
                    EasyImage.openCamera(AddOdpadekActivity.this,REQUEST_CODE_CAMERA);
                }else if(items[i].equals("Gallery")){
                    EasyImage.openGallery(AddOdpadekActivity.this, REQUEST_CODE_GALLERY);
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                switch (type){
                    case REQUEST_CODE_CAMERA:
                        Glide.with(AddOdpadekActivity.this)
                                .load(imageFile)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageview);
                        isImageUploaded = true;
                        //tvPath.setText(imageFile.getAbsolutePath());
                        mImageFile = imageFile;
                        fileUri = Uri.fromFile(imageFile);
                        break;
                    case REQUEST_CODE_GALLERY:
                        Glide.with(AddOdpadekActivity.this)
                                .load(imageFile)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imageview);
                        isImageUploaded = true;
                        //tvPath.setText(imageFile.getAbsolutePath());
                        mImageFile = imageFile;
                        fileUri = Uri.fromFile(imageFile);
                        break;
                }
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


}
