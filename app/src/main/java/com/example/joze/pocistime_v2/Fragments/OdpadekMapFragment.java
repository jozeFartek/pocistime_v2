package com.example.joze.pocistime_v2.Fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.example.joze.pocistime_v2.ApplicationMy;
import com.example.joze.pocistime_v2.Data.Odpadek;
import com.example.joze.pocistime_v2.Data.Tag;
import com.example.joze.pocistime_v2.R;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateOdpadek;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;


/**
 * Created by User on 2/28/2017.
 */



public class OdpadekMapFragment extends Fragment   implements OnMapReadyCallback {
    public static String TITLE = "ZEMLJEVID";
    private static final String TAG = "OdpadekMapFragment";

    ApplicationMy app;
    private Odpadek odpadek;

    private MapView mapView;
    private GoogleMap googleMap;




    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventUpdateOdpadek event) {
        if(event.getOdpadek().getId() == -1)
            return;
        odpadek = event.getOdpadek();
        showData();
        loadMarker();
      // Log.i(TAG, odpadek.getDescription());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_odpadek_map,container,false);

        app = (ApplicationMy) getActivity().getApplication();
        odpadek = app.getOdpadek();
        if(odpadek == null)
            odpadek = new Odpadek();


        mapView = (MapView) view.findViewById(R.id.odapdek_fragment_map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);//when you already implement OnMapReadyCallback in your fragment



        return view;
    }


    // GOOGLE MAP
    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;

        if(odpadek != null)
            loadMarker();

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            // Show rationale and request permission.
        }
    }

    private void loadMarker() {
        googleMap.clear();
        LatLng coordinate = new LatLng( odpadek.getLatitude(), odpadek.getLongitude());
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 11);
        googleMap.addMarker(new MarkerOptions().position(coordinate).title(odpadek.getName()));
        //map.animateCamera(yourLocation);
        googleMap.moveCamera(yourLocation);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    void showData()
    {

    }



}
