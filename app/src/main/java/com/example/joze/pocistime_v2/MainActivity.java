package com.example.joze.pocistime_v2;

import android.Manifest;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.joze.pocistime_v2.Data.Uporabnik;
import com.example.joze.pocistime_v2.Fragments.ListFragment;
import com.example.joze.pocistime_v2.Fragments.MapFragment;
import com.example.joze.pocistime_v2.Utils.GPSTracker;
import com.example.joze.pocistime_v2.eventbus.MessageEventSortData;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateLocation;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateOdpadki;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static Integer FRAGMENT_LIST = 0;
    private static Integer FRAGMENT_MAP = 1;
    private int currentFragment = 0;


    ApplicationMy app;

    TextView tv_userName;
    TextView tv_userEmail;
    //ImageView iv_userImage;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventUpdateLocation event) {
        app.mLocation = event.getLocation();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /** ADD NEW ODPADEK */
               addNewLocation();

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        /** NALOŽI ApplicationMy */
        app = (ApplicationMy) getApplication();

        /** NALOŽI GUI SPREMENLJIVKE */
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        /** NALOŽI GUI SPREMENLJIVKE */
        initNavigationView(navigationView);


        /** NALOŽI PRIMEREN FRAGMENT */
        loadFragment();


        /** ASK PERMISSIONS*/
        getPermissions();


        sendNotification();
    }

    private void initNavigationView(NavigationView navigationView) {
        View header = navigationView.getHeaderView(0);
        tv_userName = header.findViewById(R.id.nav_textview_name);
        tv_userEmail = header.findViewById(R.id.nav_textview_email);
       // iv_userImage = header.findViewById(R.id.nav_imageView);

        MenuItem itemLogin = navigationView.getMenu().getItem(3);
        setNavLoginItemData(itemLogin);

        tv_userEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer userId = app.getLoginId();
                if(userId == -1){
                    Intent i = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(i);
                }
            }
        });
    }


    void addNewLocation()
    {
        if(app.getLoginId() == -1) {
            Toast.makeText(getApplicationContext(),
                    "Potrebna je prijava.", Toast.LENGTH_LONG)
                    .show();
            return;
        }

        Intent i = new Intent(getBaseContext(), AddOdpadekMapActivity.class);
        startActivity(i);

    }

        private void getPermissions() {
        // APP PERMISIONS
        //ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        //ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);



        MultiplePermissionsListener my  = new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (!report.areAllPermissionsGranted()) {
                    new android.app.AlertDialog.Builder(MainActivity.this)
                            .setTitle(getString(R.string.permission_must_title))
                            .setMessage(getString(R.string.permission_must))
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    MainActivity.this.finish(); // end app!
                                }
                            })
                            .setIcon(R.mipmap.ic_launcher)
                            .show();
                }}
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        };


            Dexter.withActivity(this)
                    .withPermissions(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.CAMERA
                    ).withListener(my).check();
        }



    @Override
    protected void onResume() {
        super.onResume();
        app.requstServerOdpadki();

        if(!isMyServiceRunning(GPSTracker.class))
            startService(new Intent(app, GPSTracker.class));

        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }



    private void changeFragmnet(){
        if(currentFragment == 0)
            currentFragment = 1;
        else
            currentFragment = 0;

        loadFragment();
    }

    private void loadFragment() {
        if (findViewById(R.id.fragment_container) != null) {
            if(currentFragment == 0) {
                ListFragment listFragment = new ListFragment();
                listFragment.setArguments(getIntent().getExtras());
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, listFragment).commit();
            }
            else if(currentFragment == 1) {
                MapFragment mapFragment = new MapFragment();
                mapFragment.setArguments(getIntent().getExtras());
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mapFragment).commit();
            }


        }
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_show_list) {
            item.setChecked(true);
            currentFragment = 0;
            loadFragment();
            return true;
        }
        if (id == R.id.action_show_map) {
            item.setChecked(true);
            currentFragment = 1;
            loadFragment();
            return true;
        }

        if (id == R.id.action_filter) {
            Log.i("MAIN", "sort1");
            app.sortChangeAndUpdate();
            Log.i("MAIN", "sort2");
            int type = app.sortType;
            EventBus.getDefault().post(new MessageEventSortData(app.sortType));
            Log.i("MAIN", "sort3");
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            addNewLocation();
        }
        else if (id == R.id.nav_my) {
            Intent i = new Intent(MainActivity.this, OdpadkiUserActivity.class);
            startActivity(i);
        }
        else if (id == R.id.nav_liked) {
            Intent i = new Intent(MainActivity.this, FavoritesActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_share) {
            share();
        }
        else if (id == R.id.nav_userAction) {
            handleNavLogin(item);
        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void share()
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Preizkusi novo aplikacijo za registriranje divjih odlagališč v sloveniji: https://play.google.com/store/apps/details?id=joze.com.pocisti_me");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void handleNavLogin(MenuItem item) {
        int userId = app.getLoginId();
        if(userId != -1){
            // ODJAVA
            app.Logout();
            setNavLoginItemData(item);
        }
        else
        {
            // PRIJAVA
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
        }
    }

    private void setNavLoginItemData(MenuItem item) {
        int userId = app.getLoginId();
        if(userId == -1){
            item.setTitle("Prijava");
            tv_userName.setVisibility(View.INVISIBLE);
          //  iv_userImage.setVisibility(View.INVISIBLE);
            tv_userEmail.setText("Klikni tukaj, za prijavo");
        }

        else{
            item.setTitle("Odjava");
            Uporabnik uporabnik = app.getUserData();
            tv_userName.setVisibility(View.VISIBLE);
          //  iv_userImage.setVisibility(View.VISIBLE);
            tv_userName.setText(uporabnik.getNickName());
            tv_userEmail.setText(uporabnik.getEmail());

        }

    }




    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    public ApplicationMy getApp() {
        return app;
    }

    public void setApp(ApplicationMy app) {
        this.app = app;
    }


    public void sendNotification() {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        //Create the intent that’ll fire when the user taps the notification//

        Intent intent = new Intent(getBaseContext(), SplashScreen.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        mBuilder.setContentIntent(pendingIntent);

        mBuilder.setSmallIcon(R.mipmap.ic_launcher_round);
        mBuilder.setContentTitle("Nova lokacija");
        mBuilder.setContentText("Preveri nove lokacije v okolici.");

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(001, mBuilder.build());
    }
}
