package com.example.joze.pocistime_v2;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.joze.pocistime_v2.Data.Comment;
import com.example.joze.pocistime_v2.Data.MyResponse;
import com.example.joze.pocistime_v2.Data.Uporabnik;
import com.example.joze.pocistime_v2.Utils.ApiClient;
import com.example.joze.pocistime_v2.Utils.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCommentActivity extends AppCompatActivity {
    static String TAG = "AddComment";

    EditText et_text;
    Button btn_add;
    private ProgressDialog pDialog;

    int odpadek_id = -1;
    int uporabnik_id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Nov komentar");


        // EXTRA
        Bundle extras = getIntent().getExtras();
        odpadek_id = extras.getInt(ApplicationMy.ODPADEK_ID);
        uporabnik_id = extras.getInt(ApplicationMy.UPORABNIK_ID);

        initGUI();
    }

    private void initGUI() {
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        et_text = (EditText) findViewById(R.id.editText_addComment);
        btn_add = (Button) findViewById(R.id.btn_addComment_add);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
                String content = et_text.getText().toString();
                AddComment(content);
            }
        });
    }

    private void AddComment(String content) {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Comment comment = new Comment(uporabnik_id, odpadek_id, content);
        Call<MyResponse> call = apiService.addComment(comment);

        Log.i("MY_APP", "step1");
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                Log.i("MY_APP", "step2");
                int statusCode = response.code();
                MyResponse myResponse = response.body();
                hideDialog();
                if(myResponse.getStatusCode() == 200)
                    finish();
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                // Log error here since request failed
                Log.i("MY_APP", "step3");
                Log.e(TAG, t.toString());
                hideDialog();
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
