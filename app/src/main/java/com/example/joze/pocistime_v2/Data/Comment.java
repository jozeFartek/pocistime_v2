package com.example.joze.pocistime_v2.Data;

/**
 * Created by Joze on 4. 12. 2017.
 */

public class Comment {
    int id;
    int uporabnikId;
    int odpadekId;
    String content;
    long dateCreated;
    String userNickName;
    String userImagepath;


    public Comment(int uporabnikId, int odpadekId, String content) {
        this.uporabnikId = uporabnikId;
        this.odpadekId = odpadekId;
        this.content = content;
    }

    public Comment(int id, int uporabnikId, int odpadekId, String content, long dateCreated, String userNickName, String userImagepath) {
        this.id = id;
        this.uporabnikId = uporabnikId;
        this.odpadekId = odpadekId;
        this.content = content;
        this.dateCreated = dateCreated;
        this.userNickName = userNickName;
        this.userImagepath = userImagepath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUporabnikId() {
        return uporabnikId;
    }

    public void setUporabnikId(int uporabnikId) {
        this.uporabnikId = uporabnikId;
    }

    public int getOdpadekId() {
        return odpadekId;
    }

    public void setOdpadekId(int odpadekId) {
        this.odpadekId = odpadekId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getUserImagepath() {
        return userImagepath;
    }

    public void setUserImagepath(String userImagepath) {
        this.userImagepath = userImagepath;
    }
}
