package com.example.joze.pocistime_v2.Fragments;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.bumptech.glide.Glide;
import com.example.joze.pocistime_v2.ApplicationMy;
import com.example.joze.pocistime_v2.Data.Odpadek;
import com.example.joze.pocistime_v2.Data.Tag;
import com.example.joze.pocistime_v2.R;
import com.example.joze.pocistime_v2.Utils.ApiClient;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateOdpadek;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;


/**
 * Created by User on 2/28/2017.
 */

public class OdpadekInfoFragment extends Fragment {
    public static String TITLE = "OSNOVNO";
    private static final String TAG = "OdpadekInfoFragment";

    ApplicationMy app;

    private Odpadek odpadek;

    private ImageView image;
    private TextView tv_location, tv_time, tv_description;
    ChipCloud chipCloud;
    private FloatingActionButton fab_like;
    private  boolean isLiked = false;
    private  boolean isClicable = true;
    private  int clicableStep = 1;
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventUpdateOdpadek event) {
        odpadek = event.getOdpadek();
        showData();
        Log.i(TAG, odpadek.toString());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_odpadek_info,container,false);
        image = (ImageView) view.findViewById(R.id.odpadekInfo_image);
        tv_location = (TextView) view.findViewById(R.id.odpadekInfo_location);
        tv_time = (TextView) view.findViewById(R.id.odpadekInfo_time);
        tv_description = (TextView) view.findViewById(R.id.odpadekInfo_description);
        chipCloud = (ChipCloud) view.findViewById(R.id.chip_cloud);
        fab_like = (FloatingActionButton) view.findViewById(R.id.odpadekInfo_fab_like);

        app = (ApplicationMy) getActivity().getApplication();
        odpadek = app.getOdpadek();
            if(odpadek == null)
                odpadek = new Odpadek();
        SetLikeFab();
        fab_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(app.getLoginId() == -1) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Potrebna je prijava.", Toast.LENGTH_LONG)
                            .show();
                    return;
                }

                odpadek.setLiked(!odpadek.isLiked());
                if(odpadek.isLiked())
                    app.requstServerAddLike(odpadek.getId());
                else
                    app.requstServerRemoveLike(odpadek.getId());

                SetLikeFab();
            }
        });
        new ChipCloud.Configure()
                .chipCloud(chipCloud)
                .selectedColor(Color.parseColor("#388e3c"))
                .selectedFontColor(Color.parseColor("#ffffff"))
                .deselectedColor(Color.parseColor("#388e3c"))
                .deselectedFontColor(Color.parseColor("#ffffff"))
                .selectTransitionMS(500)
                .deselectTransitionMS(250)
                .labels(new String[]{})
                .mode(ChipCloud.Mode.MULTI)
                .allCaps(false)
                .gravity(ChipCloud.Gravity.CENTER)
                .textSize(getResources().getDimensionPixelSize(R.dimen.default_textsize))
                .verticalSpacing(getResources().getDimensionPixelSize(R.dimen.vertical_spacing))
                .minHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.min_horizontal_spacing))
                .chipListener(new ChipListener() {
                    @Override
                    public void chipSelected(int index) {

                    }
                    @Override
                    public void chipDeselected(int index) {
                        chipCloud.setSelectedChip(index);
                        //...
                    }
                })
                .build();
        return view;
    }

    private void SetLikeFab() {
        int userId = app.getLoginId();
        if(userId == -1){
            return;
        }


        if(odpadek.isLiked())
        {
            fab_like.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.fabLike)));

        }
        else
        {
            fab_like.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.fabNotLiked)));

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        showData();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    void showData()
    {

        Boolean load = false;
        if(odpadek.getFilePath() != null)
        {
            if(!odpadek.getFilePath().equals(""))
            {
                try {
                    Glide.with(getActivity())
                            .load(ApiClient.BASE_URL + "/api/Images/" + odpadek.getId() + "/" )
                            .into(image);
                    load = true;
                }
                catch (Exception e) {}
            }
        }

        if(!load)
        {
            Glide.with(getActivity())
                    .load(R.drawable.no_image)
                    .into(image);
        }

        tv_location.setText(odpadek.getLocationAddress());
        tv_time.setText(parseDate(odpadek.getDateCreated()));
        tv_description.setText(odpadek.getDescription());

        chipCloud.removeAllViews();
        List<Tag> tags = odpadek.getTags();
        for (int i = 0; i < tags.size(); i++) {
            Tag tag = tags.get(i);
            chipCloud.addChip(tag.getName());
            chipCloud.setSelectedChip(i);
        }

        SetLikeFab();
    }


    String parseDate(long timestamp){
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp * 1000L);
        String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();

        return date;
    }
}
