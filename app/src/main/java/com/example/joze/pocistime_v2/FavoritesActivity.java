package com.example.joze.pocistime_v2;

import android.location.Location;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.joze.pocistime_v2.Adapter.OdpadkiAdapter;
import com.example.joze.pocistime_v2.Adapter.OdpadkiLikedAdapter;
import com.example.joze.pocistime_v2.Data.Odpadek;
import com.example.joze.pocistime_v2.Data.Uporabnik;
import com.example.joze.pocistime_v2.Utils.ApiClient;
import com.example.joze.pocistime_v2.Utils.ApiInterface;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateLocation;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateOdpadki;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritesActivity extends AppCompatActivity {

    ApplicationMy app;

    private RecyclerView mRecyclerView;
    private OdpadkiLikedAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    TextView tvNoFavorite;

    public ArrayList<Odpadek> odpadki;

    SwipeRefreshLayout swipeRefreshLayout;


    Location mLocation;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventUpdateLocation event) {
        Log.e("TAG","new Location ListFragment");
        mLocation = event.getLocation();
        mAdapter.setLastLocation(mLocation);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        getSupportActionBar().setTitle("Shranjeno");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        app = (ApplicationMy) getApplication();
        app.odpadki = new ArrayList<>();
        mLocation = app.getLocation();

        initGUI();
    }


    void initGUI()
    {
        tvNoFavorite = (TextView) findViewById(R.id.favorite_noLiked_text);
        tvNoFavorite.setVisibility(View.INVISIBLE);

        mRecyclerView = (RecyclerView) findViewById(R.id.favorite_listLiked);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new OdpadkiLikedAdapter(app);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setLastLocation(mLocation);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOdpadki();
            }
        });

    }


    public void getOdpadki(){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Uporabnik uporabnik = new Uporabnik( app.getLoginId());
        Call<ArrayList<Odpadek>> call = apiService.getOdpadkiLiked(uporabnik);
        call.enqueue(new Callback<ArrayList<Odpadek>>() {
            @Override
            public void onResponse(Call<ArrayList<Odpadek>> call, Response<ArrayList<Odpadek>> response) {
                int statusCode = response.code();
                odpadki = response.body();
                app.odpadkiLiked = odpadki;

                mAdapter = new OdpadkiLikedAdapter(app);
                mRecyclerView.setAdapter(mAdapter);

                if(odpadki.size() == 0)
                    tvNoFavorite.setVisibility(View.VISIBLE);
                else
                    tvNoFavorite.setVisibility(View.INVISIBLE);


                if(swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
                Log.i("MY_APP", response.body().toString() + "");

            }

            @Override
            public void onFailure(Call<ArrayList<Odpadek>> call, Throwable t) {
                // Log error here since request failed
                Log.e("TAG", t.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getOdpadki();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }



}
