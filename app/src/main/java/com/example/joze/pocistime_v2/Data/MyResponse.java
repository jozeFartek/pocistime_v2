package com.example.joze.pocistime_v2.Data;

/**
 * Created by Joze on 11. 01. 2018.
 */

public class MyResponse {
    int StatusCode;
    String Message;


    public MyResponse(int statusCode, String message) {
        StatusCode = statusCode;
        Message = message;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
