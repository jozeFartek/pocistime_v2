package com.example.joze.pocistime_v2;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.joze.pocistime_v2.Adapter.SectionsPageAdapter;
import com.example.joze.pocistime_v2.Fragments.OdpadekCommentsFragment;
import com.example.joze.pocistime_v2.Fragments.OdpadekInfoFragment;
import com.example.joze.pocistime_v2.Data.Odpadek;
import com.example.joze.pocistime_v2.Fragments.OdpadekMapFragment;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateOdpadek;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class OdpadekActivity extends AppCompatActivity {
    private static final String TAG = "OdpadekActivity";

    private SectionsPageAdapter mSectionsPageAdapter;
    private ViewPager mViewPager;

    private int fragmentCount = 2;

    ApplicationMy app;
    private int odpadek_id = -1;
    public Odpadek odpadek;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventUpdateOdpadek event) {
        odpadek = event.getOdpadek();
        if(odpadek != null){
            getSupportActionBar().setTitle(odpadek.getName());

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_odpadek);

        Log.d(TAG, "onCreate: Starting.");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);



        /** NALOŽI ApplicationMy */
        app = (ApplicationMy) getApplication();

    }
/*
    void changeTitle(String title){
        setTitle("adad");
    }*/


    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);

        Bundle extras = getIntent().getExtras();
        odpadek_id = extras.getInt(ApplicationMy.ODPADEK_ID);

        Log.i(TAG, odpadek_id + "");
        /** GET ODPADEK **/
        app.requstServerOdpadek(odpadek_id);
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new OdpadekInfoFragment(), OdpadekInfoFragment.TITLE);
        adapter.addFragment(new OdpadekMapFragment(), OdpadekMapFragment.TITLE);
        adapter.addFragment(new OdpadekCommentsFragment(), OdpadekCommentsFragment.TITLE);
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
