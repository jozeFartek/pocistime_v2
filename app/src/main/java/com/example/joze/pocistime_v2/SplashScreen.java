package com.example.joze.pocistime_v2;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.VideoView;

public class SplashScreen extends Activity {

    VideoView videoView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        videoView = findViewById(R.id.videoView);
        try {
            Uri path = Uri.parse("android.resource://" + getPackageName() + "/" + +R.raw.intro_video);
            videoView.setVideoURI(path);
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    jump();
                }
            });
            videoView.start();
        } catch (Exception e) {
            jump();

        }
    }

    private void jump() {

        if(isFinishing())
            return;
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }
}
