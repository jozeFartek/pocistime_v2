package com.example.joze.pocistime_v2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.joze.pocistime_v2.Data.Uporabnik;
import com.example.joze.pocistime_v2.Utils.ApiClient;
import com.example.joze.pocistime_v2.Utils.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnRegister;
    private Button btnLinkToLogin;
    private EditText inputFullName;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initGUI();

    }

    void initGUI()
    {
        inputFullName = (EditText) findViewById(R.id.name);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String name = inputFullName.getText().toString().trim();
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {
                    registerUser(name, email, password);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Vnesi mankajoče podatke!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void registerUser(final String name, final String email,
                              final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Registracija v teku...");
        showDialog();

        callRegister(name, email, password);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void doRegister(Uporabnik uporabnik)
    {
        if(uporabnik.getId() == -1){
            Toast.makeText(getApplicationContext(),
                    "Napačni podatki!", Toast.LENGTH_LONG)
                    .show();
        }
        else
        {
            SharedPreferences.Editor editor = getSharedPreferences(ApplicationMy.MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putInt(ApplicationMy.PREFS_USER_ID, uporabnik.getId());
            editor.putString(ApplicationMy.PREFS_USER_NICKNAME, uporabnik.getNickName());
            editor.putString(ApplicationMy.PREFS_USER_EMAIL, uporabnik.getEmail());
            editor.putString(ApplicationMy.PREFS_USER_IMAGE_PATH, uporabnik.getImagePath());
            editor.apply();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    public void callRegister(String nickName, String email, String password){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Uporabnik uporabnik = null;
        Call<Uporabnik> call = apiService.callRegister(new Uporabnik(nickName,email,password));
        call.enqueue(new Callback<Uporabnik>() {
            @Override
            public void onResponse(Call<Uporabnik> call, Response<Uporabnik> response) {
                int statusCode = response.code();
                Uporabnik uporabnik = response.body();
                hideDialog();
                doRegister(uporabnik);
            }

            @Override
            public void onFailure(Call<Uporabnik> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                hideDialog();
            }
        });
    }
}
