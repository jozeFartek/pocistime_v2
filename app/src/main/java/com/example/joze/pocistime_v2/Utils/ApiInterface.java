package com.example.joze.pocistime_v2.Utils;

import com.example.joze.pocistime_v2.Data.Comment;
import com.example.joze.pocistime_v2.Data.Like;
import com.example.joze.pocistime_v2.Data.Odpadek;
import com.example.joze.pocistime_v2.Data.MyResponse;
import com.example.joze.pocistime_v2.Data.Uporabnik;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Joze on 4. 12. 2017.
 */


public interface ApiInterface {
    @GET("api/Odpadek/")
    Call<ArrayList<Odpadek>> getOdpadki();

    @Headers("Content-Type: application/json")
    @POST("api/Odpadek/Liked")
    Call<ArrayList<Odpadek>> getOdpadkiLiked(@Body Uporabnik uporabnik);

    @Headers("Content-Type: application/json")
    @POST("api/Odpadek/User")
    Call<ArrayList<Odpadek>> getOdpadkiUser(@Body Uporabnik uporabnik);

    @Headers("Content-Type: application/json")
    @POST("api/Odpadek/{id}")
    Call<Odpadek> getOpdadek(@Path("id") int id, @Body Uporabnik uporabnik);
/*
    @Headers("Content-Type: application/json")
    @POST("api/User/Login")
    Call<Uporabnik> getLogin(@Body Uporabnik uporabnik);
*/

    @Headers("Content-Type: application/json")
    @POST("api/User/Login")
    Call<Uporabnik> getLogin(@Body Uporabnik uporabnik);

    @Headers("Content-Type: application/json")
    @POST("api/User/Register")
    Call<Uporabnik> callRegister(@Body Uporabnik uporabnik);

    @Headers("Content-Type: application/json")
    @POST("/api/Comment")
    Call<MyResponse> addComment(@Body Comment comment);

    @Headers("Content-Type: application/json")
    @POST("api/Like/Add")
    Call<MyResponse>  addLike(@Body Like like);

    @Headers("Content-Type: application/json")
    @POST("api/Like/Delete")
    Call<MyResponse>  removeLike(@Body Like like);



    @Headers("Content-Type: application/json")
    @POST("api/Odpadek/Add")
    Call<Odpadek>  addOdpadek(@Body Odpadek odpadek);


    @Multipart
    @POST("api/Odpadek/AddImage")
    Call<MyResponse> uploadImage(
            @Part MultipartBody.Part file
    );
 /*   @GET("movie/top_rated")
    Call<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Call<MoviesResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);
    */
}