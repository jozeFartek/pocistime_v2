package com.example.joze.pocistime_v2.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.joze.pocistime_v2.ApplicationMy;
import com.example.joze.pocistime_v2.Data.Comment;
import com.example.joze.pocistime_v2.Data.Odpadek;
import com.example.joze.pocistime_v2.R;

import java.util.List;

/**
 * Created by Joze on 31. 12. 2017.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    ApplicationMy app;
    static Context context;
    List<Comment> comments;

    public CommentAdapter(Odpadek odpadek) {
        super();

        comments = odpadek.getComments();


    }


    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout_comment, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);

        context = parent.getContext();
        return vh;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvUserName;
        public TextView tvContent;
        public TextView tvTime;

        public ViewHolder(View v) {
            super(v);
            tvUserName = (TextView) v.findViewById(R.id.textView_comment_userName);
            tvContent = (TextView) v.findViewById(R.id.textView_comment_content);
            tvTime = (TextView) v.findViewById(R.id.textView_comment_time);
         }

    }

    @Override
    public void onBindViewHolder(CommentAdapter.ViewHolder holder, int position) {
        final Comment comment = comments.get(position);


        holder.tvUserName.setText(comment.getUserNickName());
        holder.tvContent.setText(comment.getContent());

        String time = app.getTimeAgo(comment.getDateCreated());
        holder.tvTime.setText(time);


      /*  holder.tvTtitle.setText(odpadek.getName());
        holder.tvSubtitle.setText(odpadek.getDescription());

        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OdpadkiAdapter.startDView(odpadek.getId());
            }
        });
        */

    }


    @Override
    public int getItemCount(){
        return comments.size();
    }
}
