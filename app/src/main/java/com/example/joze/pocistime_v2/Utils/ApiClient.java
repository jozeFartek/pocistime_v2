package com.example.joze.pocistime_v2.Utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Joze on 4. 12. 2017.
 */

public class ApiClient {
   // public static final String BASE_URL = "http://pocistime.azurewebsites.net/";
    public static final String BASE_URL = "http://94.140.78.58:45455/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
