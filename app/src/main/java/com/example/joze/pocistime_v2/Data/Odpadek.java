package com.example.joze.pocistime_v2.Data;

import java.util.ArrayList;

/**
 * Created by Joze on 4. 12. 2017.
 */

public class Odpadek {
    int id;
    String name;
    String description;
    double latitude;
    double longitude;
    int userId;
    String filePath;
    long dateCreated;
    long dateUpdated;
    String status;
    int volumen;
    int statusId;
    String locationAddress;
    boolean isLiked;


    ArrayList<Tag> tags;
    ArrayList<Comment> comments;

    public Odpadek(){
        tags = new ArrayList<>();
        comments = new ArrayList<>();
        isLiked = false;
    }


    public Odpadek(int id, String name, String description, double latitude, double longitude, int userId, String filePath, long dateCreated, long dateUpdated, String status, int volumen, int statusId, String locationAddress, boolean isLiked, ArrayList<Tag> tags, ArrayList<Comment> comments) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.userId = userId;
        this.filePath = filePath;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
        this.status = status;
        this.volumen = volumen;
        this.statusId = statusId;
        this.locationAddress = locationAddress;
        this.isLiked = isLiked;
        this.tags = tags;
        this.comments = comments;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(long dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getVolumen() {
        return volumen;
    }

    public void setVolumen(int volumen) {
        this.volumen = volumen;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Odpadek{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", userId=" + userId +
                ", filePath='" + filePath + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateUpdated=" + dateUpdated +
                ", status='" + status + '\'' +
                ", volumen=" + volumen +
                ", statusId=" + statusId +
                ", locationAddress='" + locationAddress + '\'' +
                ", isLiked=" + isLiked +
                ", tags=" + tags +
                ", comments=" + comments +
                '}';
    }
}
