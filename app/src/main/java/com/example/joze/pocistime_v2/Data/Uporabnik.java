package com.example.joze.pocistime_v2.Data;

/**
 * Created by Joze on 8. 01. 2018.
 */

public class Uporabnik {
    int id;
    String nickName;
    String email;
    String password;
    String imagePath;
    long dateCreated;
    int active;


    public Uporabnik(int id) {
        this.id = id;
    }

    public Uporabnik(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Uporabnik(String nickName, String email, String password) {
        this.nickName = nickName;
        this.email = email;
        this.password = password;
    }

    public Uporabnik(int id, String nickName, String email, String imagePath) {
        this.id = id;
        this.nickName = nickName;
        this.email = email;
        this.password = password;
        this.imagePath = imagePath;
    }

    public Uporabnik(int id, String nickName, String email, String password, String imagePath, long dateCreated, int active) {
        this.id = id;
        this.nickName = nickName;
        this.email = email;
        this.password = password;
        this.imagePath = imagePath;
        this.dateCreated = dateCreated;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }


    @Override
    public String toString() {
        return "Uporabnik{" +
                "id=" + id +
                ", nickName='" + nickName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", imagePath='" + imagePath + '\'' +
                ", dateCreated=" + dateCreated +
                ", active=" + active +
                '}';
    }
}
