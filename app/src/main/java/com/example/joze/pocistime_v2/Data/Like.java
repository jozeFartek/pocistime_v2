package com.example.joze.pocistime_v2.Data;

/**
 * Created by Joze on 11. 01. 2018.
 */

public class Like {
    int id;
    int odpadekId;
    int uporabnikId;


    public Like(int id, int odpadekId, int userId) {
        this.id = id;
        this.odpadekId = odpadekId;
        this.uporabnikId = userId;
    }

    public Like(int odpadekId, int userId) {
        this.odpadekId = odpadekId;
        this.uporabnikId = userId;
    }

    public int getOdpadekId() {
        return odpadekId;
    }

    public int getUporabnikIdId() {
        return uporabnikId;
    }

    @Override
    public String toString() {
        return "Like{" +
                "id=" + id +
                ", odpadekId=" + odpadekId +
                ", userId=" + uporabnikId +
                '}';
    }
}

