package com.example.joze.pocistime_v2.eventbus;

import android.location.Location;
import android.util.Log;

/**
 * Created by crepinsek on 14/04/17.
 */

public class MessageEventSortData {
    static String TAG = "MessageEventSortData";

    int sortType = 1;

    public MessageEventSortData(int sortType) {
        this.sortType = sortType;

    }

    public int getSortType() {
        return sortType;
    }
}
