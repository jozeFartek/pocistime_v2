package com.example.joze.pocistime_v2.eventbus;

import android.location.Location;
import android.util.Log;

/**
 * Created by crepinsek on 14/04/17.
 */

public class MessageEventUpdateLocation {
    static String TAG = "MessageEventUpdateLocation";
    Location location;


    public MessageEventUpdateLocation(Location m) {
        this.location = m;
        Log.i(TAG, "location: " + location.getLatitude() + " ; " + location.getLongitude());
    }

    public Location getLocation() {
        return location;
    }
}
