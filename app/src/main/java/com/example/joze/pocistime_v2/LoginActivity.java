package com.example.joze.pocistime_v2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.joze.pocistime_v2.Data.Uporabnik;
import com.example.joze.pocistime_v2.Utils.ApiClient;
import com.example.joze.pocistime_v2.Utils.ApiInterface;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    /***
     * https://www.androidhive.info/2012/01/android-login-and-registration-with-php-mysql-and-sqlite/
     */

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnLogin;
    private Button btnLinkToRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
       /* ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        */

        initGUI();


    }

    void initGUI(){
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {
                    // login user
                    checkLogin(email, password);
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Vnesi mankajoče podatke!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        // Link to Register Screen
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });
        

    }

    private void checkLogin(String email, String password) {
        pDialog.setMessage("Prijava v teku...");
        showDialog();

        requstLogin(email, password);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void doLoging(Uporabnik uporabnik)
    {
        if(uporabnik.getId() == -1){
            Toast.makeText(getApplicationContext(),
                    "Napačni podatki!", Toast.LENGTH_LONG)
                    .show();
        }
        else
        {
            SharedPreferences.Editor editor = getSharedPreferences(ApplicationMy.MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putInt(ApplicationMy.PREFS_USER_ID, uporabnik.getId());
            editor.putString(ApplicationMy.PREFS_USER_NICKNAME, uporabnik.getNickName());
            editor.putString(ApplicationMy.PREFS_USER_EMAIL, uporabnik.getEmail());
            editor.putString(ApplicationMy.PREFS_USER_IMAGE_PATH, uporabnik.getImagePath());
            editor.apply();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    public void requstLogin(String email, String password){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Uporabnik uporabnik = new Uporabnik(email,password);
        Log.i("MY_APP", uporabnik.toString());
        Call<Uporabnik> call = apiService.getLogin(uporabnik);

        Log.i("MY_APP", "step1");
        call.enqueue(new Callback<Uporabnik>() {
            @Override
            public void onResponse(Call<Uporabnik> call, Response<Uporabnik> response) {
                Log.i("MY_APP", uporabnik.toString());
                int statusCode = response.code();
                Uporabnik uporabnik = response.body();
                hideDialog();
                doLoging(uporabnik);
            }

            @Override
            public void onFailure(Call<Uporabnik> call, Throwable t) {
                // Log error here since request failed
                Log.i("MY_APP", "step3");
                Log.e(TAG, t.toString());
                hideDialog();
            }
        });
    }


}
