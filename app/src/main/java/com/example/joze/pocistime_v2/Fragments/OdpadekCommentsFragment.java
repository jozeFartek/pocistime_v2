package com.example.joze.pocistime_v2.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.joze.pocistime_v2.Adapter.CommentAdapter;
import com.example.joze.pocistime_v2.Adapter.OdpadkiAdapter;
import com.example.joze.pocistime_v2.AddCommentActivity;
import com.example.joze.pocistime_v2.ApplicationMy;
import com.example.joze.pocistime_v2.Data.Comment;
import com.example.joze.pocistime_v2.Data.Odpadek;
import com.example.joze.pocistime_v2.MainActivity;
import com.example.joze.pocistime_v2.OdpadekActivity;
import com.example.joze.pocistime_v2.R;
import com.example.joze.pocistime_v2.eventbus.MessageEventUpdateOdpadek;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

/**
 * Created by User on 2/28/2017.
 */

public class OdpadekCommentsFragment extends Fragment {
    public static String TITLE = "KOMENTARJI";
    private static final String TAG = "OdpadekCommentsFragment";


    private RecyclerView mRecyclerView;
    private CommentAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;

    public ArrayList<Comment> comments;
    Button btnAddComment;
    Odpadek odpadek;

    ApplicationMy app;
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventUpdateOdpadek event) {
        odpadek = event.getOdpadek();

        mAdapter = new CommentAdapter(odpadek);
        mRecyclerView.setAdapter(mAdapter);
//        Log.i(TAG, odpadek.getDescription());
        if(swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_odpadek_comments,container,false);


        app = (ApplicationMy) getActivity().getApplication();
        odpadek = app.getOdpadek();
        if(odpadek == null){
            odpadek = new Odpadek();
            comments = new ArrayList<>();
            odpadek.setComments(comments);
        }




        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(odpadek.getId() != -1)
                 app.requstServerOdpadek(odpadek.getId());
            }
        });
        mRecyclerView = (RecyclerView) view.findViewById(R.id.comment_recycleview);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CommentAdapter(odpadek);
        mRecyclerView.setAdapter(mAdapter);

       initGUI(view);

        return view;
    }

    private void initGUI(View view) {
        btnAddComment = (Button) view.findViewById(R.id.button_addComment);
        if(app.getLoginId() == -1)
            btnAddComment.setVisibility(View.INVISIBLE);

        btnAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AddCommentActivity.class);
                i.putExtra(ApplicationMy.ODPADEK_ID,  odpadek.getId());
                i.putExtra(ApplicationMy.UPORABNIK_ID, app.getLoginId());
                startActivity(i);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
