package com.example.joze.pocistime_v2.eventbus;

import com.example.joze.pocistime_v2.Data.Odpadek;

import java.util.ArrayList;

/**
 * Created by Joze on 5. 12. 2017.
 */

public class MessageEventUpdateOdpadki {
    public ArrayList<Odpadek> odpadki;

    public MessageEventUpdateOdpadki(ArrayList<Odpadek> odpadki) {

        this.odpadki = odpadki;
    }
    public ArrayList<Odpadek> getOdpadki() {
        return odpadki;
    }
}
